## BetterDefaultGlasscord
A Discord theme with easy config and sleek user UI and design. With support for multiple clients & glasscord.

## INFO

BetterDefaultGlasscord Improves/Fixes Glasscords Buggy Basic CSS

- Any Issues DM Leeprky#2063 Or Join The Discord
  Hope You Enjoy!

## How To Install BetterDefaultGlasscord

Open CMD & Copy This

cd powercord/src/Powercord/themes && git clone https://github.com/leeprky/BetterDefaultGlasscord

> Refresh Discord

## License

MIT, see LICENSE.md for more details

## Previews

![preview](./previews/BeytBetterDefaultGlasscord1.jpg)
![preview](./previews/BetterDefaultGlasscord2.jpg)
